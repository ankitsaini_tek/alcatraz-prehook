source = ["./gitleaks","./gitleaks-arm64"]
bundle_id = "com.tekion.security"

apple_id {
  username = "@env:AC_EMAIL"
  password = "@env:AC_PASSWORD"
}

sign {
  application_identity = "Developer ID Application: Tekion Corp"
}

zip {
  output_path = "gitleaks.zip"
}
